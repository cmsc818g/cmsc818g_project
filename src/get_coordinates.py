import json
import requests
import sys


def get_coordinates(place):
    api_key = 'AIzaSyCzYRjWHRHJ0ePeEN7KmfCTllKBp6FzoUk'
    r = requests.get('https://maps.googleapis.com/maps/api/geocode/json?address={0}&key={1}'.format(place, api_key))
    api_response_dict = r.json()
    source_coordinates = (0, 0)
    # Assemble the URL and query the web service

    # Check the HTTP status code returned by the server. Only process the response,
    # if the status code is 200 (OK in HTTP terms).
    if r.status_code != 200:
        print('HTTP status code {} received, program terminated.'.format(r.status_code))
    else:
        try:
            if len(api_response_dict['results']) > 0:
                latitude = api_response_dict['results'][0]['geometry']['location']['lat']
                longitude = api_response_dict['results'][0]['geometry']['location']['lng']
            else:
                latitude = 0
                longitude = 0
            source_coordinates = (latitude, longitude)
        # print source_coordinates
        except ValueError:
            print('Error while parsing JSON response, program terminated.')
    if sys.flags.interactive:
        from pprint import pprint
    return source_coordinates
