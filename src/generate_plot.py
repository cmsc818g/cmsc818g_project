"""
Code designed for final project for CMSC818G
-----------------

This script shows how to sample points from a Hidden Markov Model (HMM). It takes in 3 parameters:

1.) startprob: We receive a list of size 1 x N where N is the number of events in the format [('event1', probability1), ('event2', probability2), ('event2', probability2).....]
               This vector is later formatted to be [probablity1, probablity2, probablity3....]

2.) transmat: We receive a list of size 1 x N^2. We format is to become a matrix of N x N.

3.) event_name: vector of strings containing the event names.

4.) boolean(True/False): Whether you want truncation or not. Default set to False.
                         If true, code will ask how many events you want to use. The code will then automatically select
                         the top N events with highest probabilities and plot those only.

It generates plot show the sequence of observations generated with the transitions
between them.
"""
# print(__doc__)

import numpy as np
import matplotlib.pyplot as plt
from hmmlearn import hmm
import hmm_inputs
from random import randint
import time

import csv
import json
from collections import Counter, OrderedDict
import matplotlib
import itertools


def create_plot(startprob, transmat, event_name, file_name=None, truncate_events=False):
    ##############################  COMPUTE START PROBABILITIES  ##########################################

    start_prob = []  # This is going to be the final vector of start probabilities

    # Converting startprob (Nirat's fomat) into start_prob (HMMlearn's format)
    for item in startprob:
        start_prob.append(item[1])

    # Storing the size of the start probability vector
    size_of_transmat_square = len(start_prob)

    # Sanity check for starting probabilities
    print("\n Sanity Check for Start Probabilities: ", start_prob, "of size: ", len(startprob))
    time.sleep(1)

    ##############################  COMPUTE TRANSITION PROBABILITIES  ##########################################

    trans_mat = []  # This is going to be the final vector of start probabilities

    # Converting transmat (Nirat's fomat) into trans_mat (HMMlearn's format)

    # 1.) First remove all the strings
    for item in transmat:
        trans_mat.append(item[1])

    # 2.) Convert 1 x N^2 list to N x N list of lists
    i = 0
    new_list = []
    while i < len(trans_mat):
        new_list.append(trans_mat[i:i + size_of_transmat_square])
        i += size_of_transmat_square

    # 3.) Normalize the rows of transition matrix
    new_list1 = []
    for row in new_list:
        if np.sum(row) == 0:
            new_list1.append(np.multiply(np.divide(1, size_of_transmat_square), np.ones(size_of_transmat_square)))

        else:
            new_list1.append(np.divide(row, np.sum(row) + 0))
    trans_mat = new_list1

    # Sanity check for starting probabilities
    print("\n Sanity Check Transition Probabilities: ", trans_mat[0], "of size: ", len(trans_mat))
    time.sleep(1)

    ##############################  COMPUTE MEANS  ##########################################

    means = []
    for i in range(0, size_of_transmat_square):
        means.append([randint(1, 20), randint(1, 20)])

    ##############################  THIS SECTION IS FOR TRUNCATION  ##########################################

    if truncate_events:
        n = input("Please input the number of events you want to KEEP: ")
        n = int(n)
        which_inds = np.asarray(start_prob).argsort()[(-1 * n):][::-1]
        inds_to_delete = set(list(range(0, len(start_prob)))) - set(which_inds)
        inds_to_delete = list(inds_to_delete)
        print(which_inds)
        start_prob = [start_prob[i] for i in which_inds]  # New start probabilities
        print("\n Sanity Check for Start Probabilities: ", start_prob, "of size: ", len(startprob))
        time.sleep(1)
        size_of_transmat_square = len(start_prob)
        trans_mat = np.delete(trans_mat, inds_to_delete, 0)
        trans_mat = np.delete(trans_mat, inds_to_delete, 1)
        new_list1 = []
        for row in trans_mat:
            if np.sum(row) == 0:
                new_list1.append(np.multiply(np.divide(1, size_of_transmat_square), np.ones(size_of_transmat_square)))

            else:
                new_list1.append(np.divide(row, np.sum(row) + 0))
        trans_mat = new_list1  # New transition matrix
        # Sanity check for starting probabilities
        print("\n Sanity Check forTransition Probabilities: ", trans_mat[0], "of size: ", len(trans_mat))
        time.sleep(1)
        event_name = [event_name[i] for i in which_inds]  # New events
        means = [means[i] for i in which_inds]  # New Means

    ##############################  COMPUTE COVARIANCES  ##########################################

    covars = .5 * np.tile(np.identity(2), (size_of_transmat_square, 1, 1))

    ##############################  HMM LEARN PART   ##########################################

    # Build an HMM instance and set parameters
    print("Now building Model...")
    time.sleep(1.5)
    model = hmm.GaussianHMM(n_components=size_of_transmat_square, covariance_type="full")
    print("___Building Model Completed___")
    time.sleep(1.5)
    # Instead of fitting it from the data, we directly set the estimated
    # parameters, the means and covariance of the components
    model.startprob_ = start_prob
    # print len(start_prob)
    # print len(trans_mat)
    model.transmat_ = trans_mat
    print model.startprob_
    print model.transmat_
    model.means_ = means
    print model.means_
    model.covars_ = covars
    print model.covars_

    # Generate samples
    print("Generating Samples...")
    time.sleep(1)
    X, Z = model.sample(50)
    print("___Generated Samples___")
    time.sleep(1)
    # Plot the sampled data

    print("Plotting data...")
    time.sleep(1)
    plt.plot(X[:, 0], X[:, 1], ".-", label="observations", ms=6,
             mfc="orange", alpha=0.7)

    print X
    # Indicate the component numbers
    print("Indicating component numbers...")
    time.sleep(1)
    for i, m in enumerate(means):
        plt.text(m[0], m[1], '%s' % event_name[i], size=17, horizontalalignment='center',
                 bbox=dict(alpha=.7, facecolor='w'))

    plt.legend(loc='best')
    plt.savefig(file_name + ".png")
    plt.show()


"""
def main():
    list1 = ['a', 'b', 'c', 'd']
    list2 = ['b', 'd', 'c', 'a']
    list3 = ['c', 'c', 'a', 'e']
    list4 = ['a', 'c', 'a']
    event_name = ['airport', 'hotel', 'flight', 'origin', 'destination']
    dictionary = {'key1': list1, 'key2': list2, 'key3': list3, 'key4': list4}

    startprob = hmm_inputs.calculate_start_prob(dictionary)
    transmat = hmm_inputs.calculate_transition_prob(dictionary)

    create_plot(startprob, transmat, event_name, "hmm_plot", False)


if __name__ == '__main__':
    main()
"""
