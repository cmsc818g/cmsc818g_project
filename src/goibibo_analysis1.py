import csv
import json
from collections import Counter
import matplotlib
import matplotlib.pyplot as plt
import gensim
import hmm_inputs
import generate_plot
from generate_travel_value_list import generate_travel_value_list
from get_distance import get_distance

''' This file is different from the original file since it has some changes in rea_csv_file, 
passing parameters to distance calculator fundtion and import for required functions
-- To run this code, please make sure to change the data file names'''


def read_csv_file(file_name):
    """
    this function reads a csv file with event data. It is not generic enough for params to read for.
    :param file_name: name of the csv file to read.
    :return:
    """
    event_info = dict()
    with open(file_name, "rb") as f:
        csv_reader = csv.DictReader(f)
        count = 1
        for row in csv_reader:
            event_name = str(row["Event Name"]).strip()
            event_time = row["Event Time"].strip()
            state_name = str(row["State"]).strip()
            country_code = str(row["Country Code"]).strip()
            city_name = str(row["City"]).strip()
            postal_code = str(row["Postal Code"]).strip()
            appsflyer_id = str(row["AppsFlyer ID"]).strip()  # appsflyer id and advertising id are related. So
            # advertising id is not needed.
            ip_address = str(row["IP"])

            content_type = None
            source_city = None
            destination_city = None
            purchase_currency = None
            purchase_value = None
            travel_type = None  # this captures the domestic or international travel type. It can be DOMESTIC or
            # INTERNATIONAL

            try:
                event_value = json.loads(row["Event Value"])
            except Exception as err:
                # print "JSON load failed.. " + str(err)
                event_value = {}

            # TODO: check if hotel_score is needed. because it is not always present

            try:
                for key in event_value.keys():
                    if "_content_type" in key and event_value:
                        content_type = str(event_value[key])
                        # print source_city
                    if event_value and (key == "af_source_city" or key == "fb_source_city" and source_city == None) or (
                                key == "af_origin"):
                        # print key
                        # print event_value
                        source_city = str(event_value[key])
                        # print source_city
                    if "_source_city_code" in key and event_value:
                        source_city_code = str(event_value[key])
                    if event_value and (
                                    key == "af_destination_city" or key == "fb_destination_city" and destination_city == None) or (
                                key == "af_destination"):
                        destination_city = str(event_value[key])
                    if "_purchase_currency" in key and event_value:
                        purchase_currency = str(event_value[key])
                    if ("_purchase_value" in key) or (key == "af_price") and event_value:
                        purchase_value = str(event_value[key])
                    if content_type:
                        if "_dom" in content_type:
                            travel_type = "DOMESTIC"
                        elif "_int" in content_type:
                            travel_type = "INTERNATIONAL"

            except Exception as err:
                print 'None:', count
                count += 1
                print event_value
                print appsflyer_id
            # content_type = [key for key in event_value.keys() if "_content_type" in key and event_value]
            if len(event_name.split("_")) > 1:
                processed_event_name = '_'.join(event_name.split("_")[1:])
            else:
                processed_event_name = event_name

            info_dict = dict(processed_event_name=processed_event_name,
                             state_name=state_name, country_code=country_code, city_name=city_name,
                             postal_code=postal_code, event_value=event_value, appsflyer_id=appsflyer_id,
                             event_time=event_time, content_type=content_type, event_name=event_name,
                             source_city=source_city, source_city_code=source_city_code,
                             destination_city=destination_city, travel_type=travel_type,
                             ip_address=ip_address, purchase_value=purchase_value, purchase_category=None, distance=0,
                             distance_category=None)
            if str(appsflyer_id) in event_info:
                # print "appsflyer_id present: " + str(appsflyer_id)
                event_info_list = event_info[str(appsflyer_id)]
                event_info_list.append(info_dict)
            else:
                event_info_list = [info_dict]
            event_info[appsflyer_id] = event_info_list

    for id, value in event_info.iteritems():
        value.sort(key=lambda item: item['event_time'])

    return event_info


def create_histogram(x_values, y_values, plot_name):
    """
    this function plots the histogram and saves it.
    :param x_values: the values on the x-axis
    :param y_values: the values on the y-axis
    :param plot_name: the name of the file for the histogram plot.
    :return:
    """
    matplotlib.rcParams.update({'font.size': 4})
    plt.bar(x_values, y_values)
    plt.xticks(rotation=90)
    plt.show()
    plt.savefig(plot_name)


def source_related_info(event_info):
    """
    this function will look at how a particular advertisement source contributes to events
    :param event_info:
    :return:
    """
    source_info = dict()
    for event_name, event_information_dict in event_info.iteritems():
        appsflyer_id = event_information_dict["appsflyer_id"]
        source_info[appsflyer_id] = source_info.get(appsflyer_id, []) + [event_name]

    return source_info


def generate_price_list(event_info_dict):
    price_list_dict = dict.fromkeys(event_info_dict, '0')
    price_list = 0

    for applyflyer_id, event_info_list in event_info_dict.iteritems():
        # print applyflyer_id
        # if applyflyer_id == '1516861448300-6040423773273269382':
        #     for i in range(0, len(event_info_list)):
        #         # print len(event_info_list[i])
        #         print event_info_list[i]['purchase_value'] #= len(event_info_list[i])

        price_list = [elem["purchase_value"] for elem in event_info_list if elem["purchase_value"] is not None]
        # print price_list
        rem_dupes = []
        [rem_dupes.append(i) for i in price_list if not rem_dupes.count(i)]
        price_list = map(float, rem_dupes)
        # print price_list
        if price_list:
            value = sum(price_list)
            category = None
            if 10 <= value <= 1000:
                category = 1  # 'Cheap'
            elif 1001 <= value <= 10000:
                category = 2  # 'Mediocre'
            elif 10001 <= value <= 50000:
                category = 3  # 'Expensive'
            elif value >= 50000:
                category = 4  # 'Elite'
            else:
                category = None

            for i in range(0, len(event_info_list)):
                event_info_list[i]['purchase_value'] = value
                event_info_list[i]['purchase_category'] = category
            price_list_dict[applyflyer_id] = sum(price_list)

    return event_info_dict


def region_related_info(event_info):
    """
    this function will look at how a particular postal source contributes to events
    :param event_info:
    :return:
    """
    region_info = dict()  # currently implemented for postal code
    for event_name, event_information_dict in event_info.iteritems():
        city_name = event_information_dict["city_name"]
        region_info[city_name] = region_info.get(city_name, []) + [event_name]

    return region_info  # this can be plot on maps using GMaps API. We just need a key and then
    #  it is a few lines of code.


def generate_event_name_list(event_info_dict):
    """
    This function generates the list of events at the level for an appyflyer_id
    """
    event_list_dict = dict()
    for applyflyer_id, event_info_list in event_info_dict.iteritems():
        event_name_list = [elem["processed_event_name"] for elem in event_info_list if
                           elem["processed_event_name"]]  # even this statement can be
        # removed and changed to one line of code.
        event_list_dict[applyflyer_id] = event_name_list

    return event_list_dict


def generate_event_value_list(event_info_dict):
    """
    this function generates a map of appflyer_id and list of events from the event metadata
    :param event_info_dict:
    :return:
    """
    event_list_dict = dict()
    for applyflyer_id, event_info_list in event_info_dict.iteritems():
        event_name_list = [elem["content_type"] for elem in event_info_list if
                           elem["content_type"] is not None]  # even this statement can be
        # removed and changed to one line of code.
        event_list_dict[applyflyer_id] = event_name_list

    return event_list_dict


def generate_word2vec_model(info_dict, training_data_size, min_count=5, window=5):
    """
    This function takes in a dictionary and returns the word2vec model created.
    The default value for min_count in gensim is 5.
    """
    info_list = []
    query_word = "hotel_int"

    for id, event_list in info_dict.iteritems():
        info_list.append(event_list)
    print info_list[0]
    model = gensim.models.Word2Vec(info_list, min_count=min_count, sg=1, window=window)
    model_name = "word2vec_model_training_data_" + str(training_data_size) + "_min_count_" + str(
        min_count) + "_window_size_" + str(window)
    model.save("models/" + str(model_name))
    vocabulary = model.wv.vocab
    print vocabulary.keys()
    # print vocabulary[0:3]
    # most_similar_words = model.most_similar(positive=[query_word], negative=[], topn=5)
    # print most_similar_words
    return model


def get_similar_words(model, query_word, topn=5):
    """
    this function returns the topn similar words to the query word for a created word2vec model
    :param model: word2vec model
    :param query_word: the query word to search for similar words
    :param topn: top K words to search for. Default 5 words
    :return:
    """
    similar_word_list = []
    try:
        most_similar_words = model.most_similar(positive=[query_word], negative=[], topn=topn)
        similar_word_list = [elem[0] for elem in most_similar_words]
    except Exception as err:
        print err

    return similar_word_list


def test_word2vec_model(model, info_dict, topn=5):
    """
    This function tests the word2vec model. A better way is possible.
    :param model: the word2vec model to be tested
    :param info_dict: the information dictionary
    :param topn: the top K words used for testing
    :return:
    """
    correct_prediction_event_list = []
    total_events_counter = 0
    for id, event_list in info_dict.iteritems():
        for idx in range(0, len(event_list)):
            event = event_list[idx]
            similar_word_list = get_similar_words(model, query_word=str(event), topn=topn)
            events_in_window = event_list[idx - (topn / 2): idx + (topn / 2)]
            intersecting_events = set(events_in_window).intersection(set(similar_word_list))
            if len(intersecting_events) > 0:
                correct_prediction_event_list.append(event)
        total_events_counter += len(event_list)

    # print len(correct_prediction_event_list)
    # print total_events_counter

    accuracy_ratio = float(len(correct_prediction_event_list) / float(total_events_counter))
    accuracy_percentage = accuracy_ratio * 100
    # print "accuracy ratio: ", accuracy_ratio
    print "accuracy percentage: ", accuracy_percentage


def create_line_chart(file_name):
    """
    This function creates a line chart for training data size to the accuracy percentage. It is not generic enough - but can be done.
    :param file_name: the name of the file for the line chart created.
    :return:
    """
    x_values = [1000, 10000, 100000, 150000, 180000, 190000, 199000]
    y_values = [79.39929208, 80.22102121, 81.03863097, 81.10097978, 78.66301932, 77.37632956, 62.47906198]

    plt.plot(x_values, y_values)
    plt.xticks(x_values, ['1K', '10K', '100K', '150K', '180K', '190K', '199K'])
    plt.yticks(y_values)
    plt.xlabel("Training Data Size")
    plt.ylabel("Accuracy Percentage")
    plt.title("Effect of training data size on the accuracy percentage")
    plt.savefig(file_name)
    plt.show()


def main():
    """
    this is the driver function
    :return:
    """
    # TODO: enable logging rather than print statements.

    file_name = "/Users/janitanjaria/umd_code/cmsc818g_project/data/organic_inapp_events_train_size_1000.csv"
    # file_name = "/Users/niratsaini/Desktop/Goibibo/Project_code/src/trial_1.csv"
    # test_file_name = "/Users/niratsaini/Desktop/Goibibo/Project_code/src/organic_inapp_events_test_size_1000.csv"

    word2vec_implement = False
    hmm_implement = False
    generate_line_plot = False
    create_histogram_plot = False

    print "training data processing starting..."
    event_info = read_csv_file(file_name)
    event_list_dict = generate_event_name_list(event_info)
    event_value_list_dict = generate_event_value_list(event_info)
    # print event_value_list_dict.keys()

    # for key, value in event_info.items():
    #     if key == '1520157117569-8404892770507814444':
    #         print value

    '''price'''
    event_info_prices = generate_price_list(event_info)
    # for key, value in event_info_prices.items():
    #     if key == '1520157117569-8404892770507814444':
    #         print value


    # dist_dict = distance_calculator(travel_value_list_dict)
    # print dist_dict
    # print len(event_list_dict)
    # print len(event_value_list_dict)
    # print event_value_list_dict.items()[1][:15]
    # print event_list_dict.keys() #[3][:15]

    '''my code'''
    travel_value_list_dict = generate_travel_value_list(event_info)
    distance_input = travel_value_list_dict.items()
    event_info_prices_dist = get_distance(event_info_prices, distance_input)

    for key, value in event_info_prices_dist.items():
        if key == '1503773408653-8704902134858894355':
            print value
            break
    print "Created event set for prices and distances..."

    if word2vec_implement:
        model = generate_word2vec_model(event_value_list_dict, training_data_size=199000)

        print "training done...model created..."

        print "testing done processing starting..."
        event_info = read_csv_file(test_file_name)
        print len(event_info.keys())
        event_list_dict = generate_event_name_list(event_info)
        event_value_list_dict = generate_event_value_list(event_info)
        print len(event_list_dict)
        print len(event_value_list_dict)
        test_word2vec_model(model, event_value_list_dict)
        print "testing done..."

    if hmm_implement:
        state_plot_file_name = "/Users/niratsaini/Desktop/Goibibo/Project_code/src/hmm_plot_1000"
        start_probability = hmm_inputs.calculate_start_prob(event_value_list_dict)
        transition_probability = hmm_inputs.calculate_transition_prob(event_value_list_dict)

        event_names_list = []
        for id, event_info in event_value_list_dict.iteritems():
            event_names_list += event_info
        event_names_list = list(set(event_names_list))
        generate_plot.create_plot(startprob=start_probability, transmat=transition_probability,
                                  event_name=event_names_list, truncate_events=True, file_name=state_plot_file_name)

    if generate_line_plot:
        line_plot_file_name = "/Users/niratsaini/Desktop/Goibibo/Project_code/src/word2vec_accuracy_plot"
        create_line_chart(line_plot_file_name)

    if create_histogram_plot:
        event_counter = Counter([elem["processed_event_name"] for elem in event_info.values() if
                                 elem["processed_event_name"]])  # done for pre-processing.
        create_histogram(event_counter.keys()[0:50], event_counter.values()[0:50],
                         "event_info_count_histogram_plot_all.png")


if __name__ == "__main__":
    main()
