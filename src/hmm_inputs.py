from collections import Counter
import itertools


def calculate_start_prob(dictionary):
    all = []
    for i in dictionary.values():
        all = all + i

    all = set(all)
    all = list(all)
    start = dict.fromkeys(all, 0)
    all_events2 = []
    # for val in dictionary.values():
    #     if val:
    #         all_events2 += val[0]
    all_events = [i for i in dictionary.values()]
    print all_events
    all_events2 = [item[0] for item in all_events if item]
    start_prob = Counter(all_events2)
    num_events = float(len(dictionary))
    start_prob = {k: round(v / num_events, 2) for k, v in start_prob.items()}
    start.update(start_prob)
    start = sorted(start.items())
    return start


def calculate_transition_prob(dictionary):
    all = []
    for i in dictionary.values():
        all = all + i

    all = set(all)
    all = list(all)
    perm = [(all[0], all[0])]

    for i in range(1, len(all)):
        perm.append((all[i], all[i]))

    perm2 = list(itertools.permutations(all, 2))
    perm = perm + perm2
    trans_prob = dict.fromkeys(perm, 0)

    for i in dictionary.values():
        for j in range(0, len(i) - 1):
            s = (i[j], i[j + 1])
            trans_prob[s] = trans_prob[s] + 1
        # print i
    matrix_size = float(len(dictionary))
    trans_prob = {k: round(v / matrix_size, 2) for k, v in trans_prob.items()}
    trans_prob = sorted(trans_prob.items())

    return trans_prob


def main():
    list1 = ['a', 'b', 'c', 'd']
    list2 = ['b', 'd', 'c', 'a']
    list3 = ['c', 'c', 'a', 'e']
    list4 = ['a', 'c', 'a']
    dictionary = {'key1': list1, 'key2': list2, 'key3': list3, 'key4': list4}
    start_prob = calculate_start_prob(dictionary)
    print start_prob
    trans_prob = calculate_transition_prob(dictionary)
    print trans_prob


if __name__ == "__main__":
    main()

