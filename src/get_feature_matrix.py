import random

def get_feature_matrix(data):

    feat_mat = []

    for key, value in data.iteritems():
        row = []
        if data[key][0]['economy'] is not None:
            row.append(data[key][0]['economy'])
        else:
            data[key][0]['economy'] = 1e-6
            row.append(data[key][0]['economy'])

        if data[key][0]['event_len'] is not None:
            row.append(data[key][0]['event_len'])
        else:
            data[key][0]['event_len'] = 1e-6
            row.append(data[key][0]['event_len'])

        if data[key][0]['distance'] is not None:
            row.append(data[key][0]['distance'])
        else:
            data[key][0]['distance'] = 1e-6
            row.append(data[key][0]['distance'])

        if data[key][0]['purchase_value'] is not None:
            row.append(data[key][0]['purchase_value'])
        else:
            data[key][0]['purchase_value'] = 1e-6
            row.append(data[key][0]['purchase_value'])

        feat_mat.append(row)

        # label_list = [+1, -1]
        # output_label.append(random.choice(label_list))
    # print feat_mat
    return feat_mat







