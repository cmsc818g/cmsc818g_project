import csv
import json
from collections import Counter
import matplotlib
import matplotlib.pyplot as plt
import gensim
import hmm_inputs
import generate_plot


def generate_travel_value_list(event_info_dict):
    """
    this function generates a map of appflyer_id and list of travel_source_to_destivation trips from the event metadata
    :param event_info_dict:
    :return:
    """
    source_list_dict = dict.fromkeys(event_info_dict, '0')
    dest_list_dict = dict.fromkeys(event_info_dict, '0')
    # list_of_appflyer=[]
    # for i in dist_all_dict:
    #     list_of_appflyer.append(i[0])
    # dist_dict=dict.fromkeys(list_of_appflyer, 0)

    for applyflyer_id, event_info_list in event_info_dict.iteritems():
        source_event_name_list = [elem["source_city"] for elem in event_info_list if
                                  elem["source_city"] is not None]
        if source_event_name_list:
            source_list_dict[applyflyer_id] = source_event_name_list

    for applyflyer_id, event_info_list in event_info_dict.iteritems():
        event_name_list = [elem["destination_city"] for elem in event_info_list if
                           elem["destination_city"] is not None]
        if event_name_list:
            dest_list_dict[applyflyer_id] = event_name_list

    ds = [source_list_dict, dest_list_dict]
    d = {}
    for k in source_list_dict.iterkeys():
        d[k] = tuple(d[k] for d in ds)
    for key in d.iterkeys():
        tup = d[key]
        tup_mix = zip(tup[0], tup[1])
        rem_dupes = []
        [rem_dupes.append(i) for i in tup_mix if not rem_dupes.count(i)]
        d[key] = rem_dupes
    # print d
    return d
