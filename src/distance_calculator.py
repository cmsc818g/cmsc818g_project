import json
import requests
import sys


def distance_calculator(dist_all_dict):
    # The API key must be provided on the command line, abort otherwise.
    api_key = 'AIzaSyCzYRjWHRHJ0ePeEN7KmfCTllKBp6FzoUk'
    # Google Distance Matrix base URL to which all other parameters are attached
    base_url = 'https://maps.googleapis.com/maps/api/distancematrix/json?'

    list_of_appflyer = []
    for i in dist_all_dict:
        list_of_appflyer.append(i[0])
    dist_dict = dict.fromkeys(list_of_appflyer, 0)

    for i in dist_all_dict:
        total_dist = 0
        for j in i[1]:
            origins = [j[0]]
            destinations = [j[1]]

            # Prepare the request details for the assembly into a request URL
            payload = {
                'origins': '|'.join(origins),
                'destinations': '|'.join(destinations),
                'mode': 'driving',
                'api_key': api_key
            }

            # Assemble the URL and query the web service
            r = requests.get(base_url, params=payload)

            # Check the HTTP status code returned by the server. Only process the response,
            # if the status code is 200 (OK in HTTP terms).
            if r.status_code != 200:
                print('HTTP status code {} received, program terminated.'.format(r.status_code))
            else:
                try:
                    # Try/catch block should capture the problems when loading JSON data,
                    # such as when JSON is broken. It won't, however, help much if JSON format
                    # for this service has changed -- in that case, the dictionaries json.loads() produces
                    # may not have some of the fields queried later. In a production system, some sort
                    # of verification of JSON file structure is required before processing it. In XML
                    # this role is performed by XML Schema.
                    x = json.loads(r.text)

                    # Now you can do as you please with the data structure stored in x.
                    # Here, we print it as a Cartesian product.
                    # for isrc, src in enumerate(x['origin_addresses']):
                    for isrc, src in enumerate(x['origin_addresses']):
                        for idst, dst in enumerate(x['destination_addresses']):
                            row = x['rows'][isrc]
                            cell = row['elements'][idst]

                            if cell['status'] == 'OK':
                                dist = (cell['distance']['text'])
                                # print dist
                                print('{} to {}: {}, {}.'.format(src, dst, cell['distance']['text'],
                                                                 cell['duration']['text']))
                            else:
                                print 'There is some error'
                                dist = '0'
                                print('{} to {}: status = {}'.format(src, dst, cell['status']))

                    words = dist.split(' ')
                    d1 = words[0].replace(',', '')
                    total_dist = total_dist + int(d1)

                    # Of course, we could have also saved the results in a file,
                    with open('gdmpydemo.json', 'w') as f:
                        f.write(r.text)

                except ValueError:
                    print('Error while parsing JSON response, program terminated.')

            # Prepare for debugging, but only if interactive. Now you can pprint(x), for example.
            if sys.flags.interactive:
                from pprint import pprint

        dist_dict[i[0]] = dist_dict[i[0]] + total_dist
    return dist_dict
