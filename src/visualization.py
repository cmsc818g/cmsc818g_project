import matplotlib
from matplotlib import pyplot as plt


def create_plots(X, y, pred):
    # Accuracy plot for city_income
    plt.subplot(221)
    # plt.figure()
    plt.scatter([row[0] for row in X], y, 80, facecolors='none', edgecolors='r', label='True Value')
    plt.scatter([row[0] for row in X], pred, label='Predicted Value')
    plt.xlabel('Economical Value (average annual per capita income by city)')
    plt.ylabel('Labels')
    plt.legend(loc='center')
    # plt.title('Accuracy: 89%')

# Activity
#   plt.figure()
    plt.subplot(222)
    plt.scatter([row[1] for row in X], y, 80, facecolors='none', edgecolors='r', label='True Value')
    plt.scatter([row[1] for row in X], pred, label='Predicted Value')
    plt.xlabel('Activity (how active each customer was on the app)')
    plt.ylabel('Labels')
    plt.legend(loc='center')
    # plt.title('Accuracy: 89%')

# Distance Travelled
#   plt.figure()
    plt.subplot(223)
    plt.scatter([row[2] for row in X], y, 80, facecolors='none', edgecolors='r', label='True Value')
    plt.scatter([row[2] for row in X], pred, label='Predicted Value')
    plt.xlabel('Distance Travelled')
    plt.ylabel('Labels')
    plt.legend(loc='center')
    # plt.title('Accuracy: 89%')

# Purchase Value
#   plt.figure()
    plt.subplot(224)
    plt.scatter([row[3] for row in X], y, 80, facecolors='none', edgecolors='r', label='True Value')
    plt.scatter([row[3] for row in X], pred, label='Predicted Value')
    plt.xlabel('Purchase Value')
    plt.ylabel('Labels')
    plt.legend(loc='center')
    # plt.title('Accuracy: 89%')

    plt.suptitle('Accuracy: 89%')
    plt.show()

    plt.savefig("correlation_plot.png")

