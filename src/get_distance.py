import json
import requests
import sys
import geopy.distance
from get_coordinates import get_coordinates


def get_distance(event_info, dist_all_dict):
    list_of_appflyer = []
    for i in dist_all_dict:
        list_of_appflyer.append(i[0])
    dist_dict = dict.fromkeys(list_of_appflyer, 0)

    for i in dist_all_dict:
        total_dist = 0
        category = None
        for j in i[1]:
            origins = [j[0]]
            destinations = [j[1]]
            # total_dist=5
            if origins != ['0'] and destinations != ['0'] and origins != ['None'] and destinations != ['None']:
                # Prepare the request details for the assembly into a request URL
                co_cords = []
                places = [origins[0], destinations[0]]
                # print 'Id:', i
                # print places
                for k in range(len(places)):
                    source_address = places[k]
                    new_cords = get_coordinates(source_address)
                    co_cords.append(new_cords)

                coords_1 = co_cords[0]
                coords_2 = co_cords[1]
                dist = geopy.distance.vincenty(coords_1, coords_2).km

                total_dist = total_dist + int(dist)

        dist_dict[i[0]] = dist_dict[i[0]] + total_dist

    # print dist_dict
    for applyflyer_id, event_info_list in event_info.iteritems():
        for key in dist_dict.keys():
            if key == applyflyer_id:
                value = dist_dict[key]
                if 10 <= value <= 200:
                    category = 1
                elif 201 <= value <= 1000:
                    category = 2
                elif 1001 <= value <= 10000:
                    category = 3
                elif value >= 10001:
                    category = 4
                else:
                    category = None

                for n in range(0, len(event_info_list)):
                    event_info_list[n]['distance'] = value
                    event_info_list[n]['distance_category'] = category

    return event_info
