"""
Code designed for final project for CMSC818G
-----------------

This script "price_sense_rohan.py" contains a method called create_price_sensitivity_features(event_value_list_dict). This
takes in a list of dicts. It uses key "Pincode" to find the area and stores that in the key "Pincode_Info". It also computes
the length of each dict.

"""
# print(__doc__)

import csv
import json
from collections import Counter
import matplotlib
import matplotlib.pyplot as plt
import gensim
import hmm_inputs
import generate_plot
from six.moves import urllib
import copy
import time
import requests

dict = [{"name": "Rohan", "Pincode": 201301, 'Pincode_Info': None, 'Event_Len': None},
        {"name": "Chahat", "Pincode": 20740, 'Pincode_Info': None, 'Event_Len': None},
        {"name": "Nitin", "Pincode": 560097, 'Pincode_Info': None, 'Event_Len': None}]

urban = ['KanchiPuram', 'Delhi', 'Pune', 'Mountain View', 'Bangalore', 'Hyderabad', 'Mumbai',
         'Gurgaon', 'Miami', 'Calcutta', 'Vellore', 'Chandigarh', 'Jaipur', 'Chennai', 'Allahabad',
         'Gwalior City', 'Tirupati', 'Bikaner', 'Singapore', 'Amritsar', 'Ahmedabad', 'Surat', 'Madurai']


def only_pincodes(janits_dict):
    for key in janits_dict.keys():
        pin = janits_dict[key][0]['pincode_info']
        print(pin)


def create_price_sensitivity_features(janits_dict, city_incomes):
    # print("Fetching Pincode Info...")
    # print("Now reading in dict...")
    for key, value in janits_dict.iteritems():
        # print len(janits_dict)
        # pincode = janits_dict[key][0]["postal_code"]
        # try:
        #     r = requests.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + str(
        #         pincode) + "&sensor=true&key=AIzaSyAyJI3uvIQEoxefPvgKsLHUz-rsFGAU71A")
        #     # with urllib.request.urlopen("http://maps.googleapis.com/maps/api/geocode/json?address="+str(pincode)+"&sensor=true") as url:
        #     # data = json.loads(url.read().decode())
        #     data = json.loads(r.content)
        #     # print(data['results'])
        #     if len(data['results']) == 0:
        #         continue
        #     mystring = data['results'][0]['formatted_address']
        #     mystring = mystring.split()
        #
        #     for idx in range(0, len(value)):
        #         janits_dict[key][idx]['pincode_info'] = mystring[0] + mystring[1]
        #
        # except Exception as err:
        #     print err

        janits_dict[key][0]['event_len'] = len(janits_dict[key][0]['event_value'])

        # if janits_dict[key][0]['travel_class'] ==


        janits_dict[key][0]['economy'] = city_incomes[janits_dict[key][0]['city_name']]
    return janits_dict

# if __name__ == "__main__":
#     # dict = []
#     # dict = [{"name": "Rohan", "Pincode": 201301, 'Pincode_Info': None}, {"name": "Chahat", "Pincode": 20740, 'Pincode_Info': None},
#     #         {"name": "Nitin", "Pincode": 560097, 'Pincode_Info': None}]
#     janits_dict = copy.deepcopy(dict)
#     new_janits_dict = create_price_sensitivity_features(janits_dict)
#     print("Original Dict: \n", dict, "\n \n Dict populated with zipcode info: \n", new_janits_dict)
