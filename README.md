# cmsc818g_project

Project Members:
1. Janit Anjaria (janit@cs.umd.edu)
2. Nirat Saini (nirat@cs.umd.edu)
3. Rohan Chandra (rohan@cs.umd.edu)

Project Title:
Behavior Modeling of Travelers

Dependencies:
We used an anaconda environment. But some of the dependencies that might not be present are:
matplotlib
pandas
requests
gensim
geopy
sklearn

Steps to execute the code:
1. python goibibo_analysis.py 
This python file has flags for running various parts of the program. Currently only the logistic regression model is set to True.
